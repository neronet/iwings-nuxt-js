export default defineNuxtConfig({
  // Image module configuration
  image: {
    format: ['webp'], // Optimize image loading by using WebP format
  },
  
  // Server-Side Rendering (SSR) configuration
  ssr: true, // Enable server-side rendering for better SEO and initial page load performance
  
  app: {
    head: {
      script: [
        {
          defer: true,
          hid: 'gtmHead',
          innerHTML: `
            (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','GTM-5L4DTJQM');`,
        }
      ],
      __dangerouslyDisableSanitizers: ['script'], // Disable script sanitization (use with caution)
    }
  },

  // Feature flags
  features: {
    inlineStyles: false, // Disable inline styles to improve performance
  },

  // Development tools settings
  devtools: {
    enabled: false, // Disable development tools in production for better performance
  },

  // Global CSS settings
  css: ['~/assets/scss/main.scss'], // Include global SCSS stylesheet

  // Modules configuration
  modules: [
    'vue3-carousel-nuxt', // Carousel component module
    'nuxt-delay-hydration', // Module for delaying hydration
    '@nuxt/image', // Image optimization module
    ['nuxt-mail', {
      message: {
        to: 'info@iwings.io', // Default recipient for emails
      },
      smtp: {
        host: "smtp.zoho.com",
        port: 465,
        secure: true, // Use TLS for secure email sending
        auth: {
          user: "info@iwings.io",
          pass: "2322289Go21$iwi", // Use environment variables for sensitive data
        }
      }
    }]
  ],

  // Delay hydration settings
  delayHydration: {
    mode: 'mount' // Enable lazy hydration, delaying the hydration process until component mounts
  },

  // Robots.txt configuration
  robots: {
    UserAgent: '*',
    Disallow: '/login' // Disallow search engines from indexing the login page
  },

  // Sitemap configuration
  sitemap: {
    hostname: 'https://****/', // Set the hostname for the sitemap
    exclude: ['/forget-password'] // Exclude the forget-password page from the sitemap
  },

  // Nitro server configuration
  nitro: {
    compressPublicAssets: true // Compress public assets for better performance
  },

  // Build configuration
  build: {
    // Code splitting settings
    splitChunks: {
      layouts: true, // Split layout files
      pages: true, // Split page files
      commons: true // Split common dependencies
    },

    // Cache groups configuration
    cacheGroups: {
      vendor: {
        test: /[\\/]node_modules[\\/]/,
        name: 'vendor', // Name for the vendor chunk
        chunks: 'all' // Apply to all chunks
      }
    },

    extractCSS: true, // Extract CSS into separate files

    // Optimization settings
    optimization: {
      splitChunks: {
        cacheGroups: {
          styles: {
            name: 'styles', // Name for the styles chunk
            test: /\.(css|vue)$/, // Match CSS and Vue files
            chunks: 'all', // Apply to all chunks
            enforce: true // Enforce creation of styles chunk
          }
        }
      }
    }
  },

  // Server-side rendering settings
  render: {
    compressor: (req, res, next) => {
      const shouldCompress = req.headers['x-no-compression'] ? false : true;
      if (shouldCompress) {
        res.setHeader('Content-Encoding', 'gzip'); // Enable Gzip compression for better performance
      }
      next();
    }
  }
})
